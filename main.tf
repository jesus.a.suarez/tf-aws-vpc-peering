resource "aws_vpc_peering_connection" "vpc_peering" {
  peer_owner_id = "${var.peer_owner_id}"
  peer_vpc_id   = "${var.peer_vpc_id}"
  vpc_id        = "${var.vpc_id}"
  auto_accept   = "${var.auto_accept}"
  peer_region   = "${var.peer_region}"

  accepter {
    allow_remote_vpc_dns_resolution = "${var.allow_remote_vpc_dns_resolution}"
  }

  requester {
    allow_remote_vpc_dns_resolution = "${var.allow_remote_vpc_dns_resolution}"
  }

  tags {
    Name = "VPC Peering between ${var.vpc_name} and ${var.peer_vpc_name}"
  }
}

# Public routes for local vpc
resource "aws_route" "public_routes_vpc" {
  count                     = "${length(var.public_route_table_ids)}"
  route_table_id            = "${element(var.public_route_table_ids, count.index)}"
  destination_cidr_block    = "${var.peer_destination_cidr_block}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.vpc_peering.id}"
}

# Private routes for local vpc
resource "aws_route" "private_route_vpc" {
  count                     = "${length(var.private_route_table_ids)}"
  route_table_id            = "${element(var.private_route_table_ids, count.index)}"
  destination_cidr_block    = "${var.peer_destination_cidr_block}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.vpc_peering.id}"
}

# Public routes for peer vpc
resource "aws_route" "public_routes_vpc_peer" {
  count                     = "${length(var.peer_public_route_table_ids)}"
  route_table_id            = "${element(var.peer_public_route_table_ids, count.index)}"
  destination_cidr_block    = "${var.vpc_cidr_block}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.vpc_peering.id}"
}

# Private routes for peer vpc
resource "aws_route" "private_routes_vpc_peer" {
  count                     = "${length(var.peer_private_route_table_ids)}"
  route_table_id            = "${element(var.peer_private_route_table_ids, count.index)}"
  destination_cidr_block    = "${var.vpc_cidr_block}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.vpc_peering.id}"
}
