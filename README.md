# tf-aws-vpc-peering #

## Overview ##
This module is aimed to request a vpc peering connection between your vpc and a peer vpc in another account. The peering is established When the peer vpc accepts the request (if auto_accept is set to false).

## Usage:

```
module "vpc_peering" {
  source = "./modules/tf-aws-vpc-peering-request"

  peer_owner_id = "111111111111"
  peer_vpc_id   = "vpc-55555555"

  vpc_id         = "${module.vpc.vpc_id}"
  vpc_name       = "${var.environment}-vpc"
  vpc_cidr_block = "${module.vpc.vpc_cidr_block}"

  peer_vpc_name               = "prd-vpc"
  peer_destination_cidr_block = "10.0.0.0/16"
  auto_accept                 = true

  public_route_table_ids  = ["${module.vpc.public_route_table_id}"]
  private_route_table_ids = "${module.vpc.private_route_tables_ids}"

  peer_public_route_table_ids  = ["${data.aws_route_table.prd_public.id}"]
  peer_private_route_table_ids = "${var.prd_private_route_table_ids}"
}

```


## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| allow_remote_vpc_dns_resolution | Allow a local VPC to resolve public DNS hostnames to private IP addresses when queried from instances in the peer VPC. | string | `false` | no |
| auto_accept | Accept the peering (both VPCs need to be in the same AWS account). | string | `false` | no |
| peer_destination_cidr_block | The cidr block of the VPC with which you are creating the VPC Peering Connection. | string | - | yes |
| peer_owner_id | The AWS account ID of the owner of the peer VPC. Defaults to the account ID the AWS provider is currently connected to. | string | - | yes |
| peer_private_route_table_ids | List of private route tables the peered VPC. | string | `<list>` | no |
| peer_public_route_table_ids | List of public route tables of the peered VPC. | string | `<list>` | no |
| peer_vpc_id | The ID of the VPC with which you are creating the VPC Peering Connection. | string | - | yes |
| peer_vpc_name | The name of the f the VPC with which you are creating the VPC Peering Connection. | string | - | yes |
| private_route_table_ids | List of private route tables the requester VPC. | string | `<list>` | no |
| public_route_table_ids | List of public route tables of the requester VPC. | string | `<list>` | no |
| vpc_cidr_block | The cidr block of the requester VPC | string | `` | no |
| vpc_id | The ID of the requester VPC. | string | - | yes |
| vpc_name | The name of the requester VPC. | string | - | yes |

## Outputs

| Name | Description |
|------|-------------|
| peer_id | The ID of the VPC Peering Connection. |
