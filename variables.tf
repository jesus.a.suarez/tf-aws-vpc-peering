variable "vpc_id" {
  description = "The ID of the requester VPC."
}

variable "vpc_cidr_block" {
  description = "The cidr block of the requester VPC"
  default     = ""
}

variable "vpc_name" {
  description = "The name of the requester VPC."
}

variable "allow_remote_vpc_dns_resolution" {
  description = "Allow a local VPC to resolve public DNS hostnames to private IP addresses when queried from instances in the peer VPC."
  default     = "false"
}

variable "public_route_table_ids" {
  description = "List of public route tables of the requester VPC."
  default     = []
}

variable "private_route_table_ids" {
  description = "List of private route tables the requester VPC."
  default     = []
}

variable "peer_public_route_table_ids" {
  description = "List of public route tables of the peered VPC."
  default     = []
}

variable "peer_private_route_table_ids" {
  description = "List of private route tables the peered VPC."
  default     = []
}

variable "peer_vpc_name" {
  description = "The name of the f the VPC with which you are creating the VPC Peering Connection."
}

variable "peer_owner_id" {
  description = "The AWS account ID of the owner of the peer VPC. Defaults to the account ID the AWS provider is currently connected to."
}

variable "peer_vpc_id" {
  description = "The ID of the VPC with which you are creating the VPC Peering Connection."
}

variable "peer_region" {
  description = "Peer region."
}

variable "peer_destination_cidr_block" {
  description = "The cidr block of the VPC with which you are creating the VPC Peering Connection."
}

variable "auto_accept" {
  description = "Accept the peering (both VPCs need to be in the same AWS account)."
  default     = false
}
